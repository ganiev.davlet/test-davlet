<?php

use yii\helpers\Html;
use yii\grid\GridView;
?>
<div class="admin-default-index">
    <h1>Административная панель</h1>

    <span>
        <?= Html::a('Книги', ['./book'], ['class' => 'btn btn-success']) ?>
    </span>
    <span>
        <?= Html::a('Авторы', ['./author'], ['class' => 'btn btn-success']) ?>
    </span>
</div>