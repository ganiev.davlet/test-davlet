<?php

namespace app\modules\admin\controllers;

use Yii;
use backend\modules\admin\models\Book;
use backend\modules\admin\models\BookSearch;
use backend\modules\admin\models\Author;
use backend\modules\admin\models\AuthorSearch;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */

    public function actionIndex()
    {
        $model = new BookSearch();
        $dataProvider = $model->search($_REQUEST);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }
    public function actionListAuthor()
    {
        $model = new AuthorSearch();
        $dataProvider = $model->search($_REQUEST);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }
}
